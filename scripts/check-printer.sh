#!/bin/bash

# This script checks printer status and restarts it if it finds it disabled.

# Copy this script in a accessible location, e.g. /root/ ,
#   and setup crontab to run it every minute.
# Make sure to make the script executable.
# chmod +x /root/check-printer.sh

# You can also use `lpstat -p` to check printer status.

# crontab -e
# */1 * * * * /root/check-printer.sh


# Use the exact name you used in CUPS when you setup this printer.

PRINTER=POS80
LPSTAT=/usr/bin/lpstat
CUPS_ENABLE=/usr/sbin/cupsenable
CUPS_DISABLE=/usr/sbin/cupsdisable
PATTERN="enabled"

# Example return messages from 'lpstat -p'

# printer POS80 is idle. enabled since Tue 05 Oct 2021 07:13:50 PM BST

# printer POS80 disabled since Tue 05 Oct 2021 07:13:40 PM BST -
# Paused 


PRINTER_STATUS=$(${LPSTAT} -p | egrep 'enabled|disabled')


function echolog {
  echo $1
  echo $1 | logger  
}


# echo "PATTERN is: ${PATTERN}"
echolog "PRINTER_STATUS is: ${PRINTER_STATUS}"

if [[ "${PRINTER_STATUS}" =~ "${PATTERN}" ]]; then
  echolog "Printer - ${PRINTER} - found enabled. ALL OK. Nothing to do."
else
  echolog "Printer - ${PRINTER} - found disabled. Something is wrong."
  echolog "Trying to restart printer - ${PRINTER} ..."
  echo "Stopping printer - ${PRINTER} ..."
  ${CUPS_DISABLE} ${PRINTER}
  sleep 3
  echo "Starting printer - ${PRINTER} ..."
  ${CUPS_ENABLE} ${PRINTER}
  sleep 3 

  echolog "Printer - ${PRINTER} - restarted. Checking printer status again ..."

  PRINTER_STATUS=$(${LPSTAT} -p | egrep 'enabled|disabled')

  echolog "${PRINTER_STATUS}"

fi

