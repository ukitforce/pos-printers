# Linux/CUPS compatible POS/thermal printers and drivers:

Most of the POS printers are from the company called Scangle. There are two main types:
* 80 mm (printable area: 72 mm)
* 58 mm (printable area: 52 mm)

## 80 mm printers:
* SGT-88III
* SGT-88IV



# General Instructions:
The `drivers/` directory contains sub-directories for various models. The file "supported-models.txt" lists the printer models supported by that driver.

Each printer driver provides `filter` files, and a `ppd` files. The **filter** files are the actual *drivers*, and the **PPD** files - or **PostScript Printer Description** files - are created by vendors to describe the entire set of features and capabilities available for their PostScript printers. A PPD also contains the PostScript code (commands) used to invoke features for the print job.

The above two sets of files are compatible with CUPS (Common Unix Printing System). These can be used on any Linux distribution, such as RedHat, Fedora, Debian, etc.

## Steps:
* Connect your printer to the computer using USB connection. (Some POS terminals have built in printer, which is internally connected to the POS computer through USB connection).
* Install cups. On RedHat and derivatives, it would be `yum install cups`, followed by `systemctl enable cups`. 
* Download / clone this repository on your computer.
* Select the correct sub-directory under the `drivers/` directory, and then copy the files from `filter/` directory to `/usr/lib/cups/filter/` on your computer.
* Start the browser, and visit the URL [http://localhost:631](http://localhost:631), which will bring up CUPS web interface. Login to cups interface with your OS user credentials, and go to "Administration" -> "Add Printer". Use the name `POS80`.
* You will see a **"Unknown"** printer on USB. Select that. If it does not show as "Unknown", then turn off the printer , disconnect printer USB cable, turn on the printer and connect the USB cable again. Refresh "Add Printer" screen/web-page.
* Do not select any item from the "Make" list. Instead, press the "Choose File" button, and provide the `pos80.ppd` file. (or pos50.ppd)
* On the next screen titled **"General"**, choose various printer settings. Optionally, change **"Media Size"** from "72mmx297mm" to "72mmx100mm", or to "72mmx500mm". Change any other settings you want to change and press the "Set Default Options" button.
* Print a test page so see everything is OK.
* Set this printer as default printer.

![images/cups-pos-printer-1.png](images/cups-pos-printer-1.png)

![images/cups-pos-printer-2.png](images/cups-pos-printer-2.png)




# Firefox printer configuration:

First, find the file named `prefs.js` in the home directory of the `pos` user:

```
find . -name prefs.js
```

```
$ find . -name prefs.js
./.mozilla/firefox/ojb2zra7.default-release/prefs.js
```

Then, you can edit the `prefs.js` file directly, or simply create a new file in the same location with the name `user.js` , and add the following, adjusting the name of the printer, and margins, etc.

```
$ vi ./.mozilla/firefox/ojb2zra7.default-release/user.js

user_pref("print.always_print_silent", true);
user_pref("print.more-settings.open", true);
user_pref("print.print_bgcolor", false);
user_pref("print.print_bgimages", false);
user_pref("print.print_duplex", 0);
user_pref("print.print_evenpages", true);
user_pref("print.print_footerleft", "");
user_pref("print.print_footerright", "");
user_pref("print.print_headerleft", "");
user_pref("print.print_headerright", "");
user_pref("print.print_margin_bottom", "0");
user_pref("print.print_margin_left", "0");
user_pref("print.print_margin_right", "0");
user_pref("print.print_margin_top", "0");
user_pref("print.print_oddpages", true);
user_pref("print.print_orientation", 0);
user_pref("print.print_page_delay", 50);
user_pref("print.print_paper_data", 0);
user_pref("print.print_paper_height", "118.11");
user_pref("print.print_paper_name", "ppd_72mmx3000mm");
user_pref("print.print_paper_size_unit", 0);
user_pref("print.print_paper_width", "  2.83");
user_pref("print.print_scaling", "  1.00");
user_pref("print.print_shrink_to_fit", false);
user_pref("print.print_to_file", false);
user_pref("print.print_to_filename", "/home/pos/mozilla.pdf");
user_pref("print.print_unwriteable_margin_bottom", 0);
user_pref("print.print_unwriteable_margin_left", 0);
user_pref("print.print_unwriteable_margin_right", 0);
user_pref("print.print_unwriteable_margin_top", 0);
user_pref("print.printer_POS80.print_bgcolor", false);
user_pref("print.printer_POS80.print_bgimages", false);
user_pref("print.printer_POS80.print_duplex", 0);
user_pref("print.printer_POS80.print_edge_bottom", 0);
user_pref("print.printer_POS80.print_edge_left", 0);
user_pref("print.printer_POS80.print_edge_right", 0);
user_pref("print.printer_POS80.print_edge_top", 0);
user_pref("print.printer_POS80.print_footercenter", "");
user_pref("print.printer_POS80.print_footerleft", "");
user_pref("print.printer_POS80.print_footerright", "");
user_pref("print.printer_POS80.print_headercenter", "");
user_pref("print.printer_POS80.print_headerleft", "");
user_pref("print.printer_POS80.print_headerright", "");
user_pref("print.printer_POS80.print_in_color", true);
user_pref("print.printer_POS80.print_margin_bottom", "0");
user_pref("print.printer_POS80.print_margin_left", "0");
user_pref("print.printer_POS80.print_margin_right", "0");
user_pref("print.printer_POS80.print_margin_top", "0");
user_pref("print.printer_POS80.print_orientation", 0);
user_pref("print.printer_POS80.print_page_delay", 50);
user_pref("print.printer_POS80.print_paper_height", "14.9606");
user_pref("print.printer_POS80.print_paper_id", "custom_72x380mm_72x380mm_borderless");
user_pref("print.printer_POS80.print_paper_size_unit", 0);
user_pref("print.printer_POS80.print_paper_width", "2.83465");
user_pref("print.printer_POS80.print_resolution", 0);
user_pref("print.printer_POS80.print_reversed", false);
user_pref("print.printer_POS80.print_scaling", "1");
user_pref("print.printer_POS80.print_shrink_to_fit", false);
user_pref("print.printer_POS80.print_to_file", false);
user_pref("print.printer_POS80.print_to_filename", "");
user_pref("print.printer_POS80.print_unwriteable_margin_bottom", 0);
user_pref("print.printer_POS80.print_unwriteable_margin_left", 0);
user_pref("print.printer_POS80.print_unwriteable_margin_right", 0);
user_pref("print.printer_POS80.print_unwriteable_margin_top", 0);
user_pref("print_printer", "POS80");
```










------


# Reference links:
* http://scangle.in/down_driver.php
* http://scangle.in/sub_productdetails.php?sub_id=5
* https://scangle.en.made-in-china.com/product/UKdxFYRcADke/China-Scangle-Sgt-802-Thermal-Receipt-Printer-with-All-in-One-Interface.html
* https://download.epson-biz.com/modules/pos/index.php?page=soft&pcat=3&scat=32
* https://askubuntu.com/questions/1189080/install-epson-tm-t88v-drivers-on-ubuntu-18-04-lts
